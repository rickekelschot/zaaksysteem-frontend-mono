ARG CONTAINER_ROOT=/opt/zaaksysteem-frontend-mono

### --------- Stage: Setup NGINX for development ---------
FROM node:dubnium AS base

RUN set -x\
  && apt-get update\
  && apt-get install --no-install-recommends --no-install-suggests -y nginx

### --------- Stage: Install Node.js build dependencies ---------
FROM base AS install-dependencies

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

ENV NODE_ENV="development"

# USER node
WORKDIR $CONTAINER_ROOT

COPY --chown=node:node package.json yarn.lock lerna.json $CONTAINER_ROOT/

RUN yarn

# Apps
COPY --chown=node:node apps/main/package.json apps/main/yarn.lock $CONTAINER_ROOT/apps/main/

# Packages
COPY --chown=node:node packages/kitchen-sink/package.json $CONTAINER_ROOT/packages/kitchen-sink/
COPY --chown=node:node packages/ui/package.json $CONTAINER_ROOT/packages/ui/
COPY --chown=node:node packages/common/package.json $CONTAINER_ROOT/packages/common/

RUN yarn lerna:bootstrap

COPY --chown=node:node apps $CONTAINER_ROOT/apps
COPY --chown=node:node packages $CONTAINER_ROOT/packages

### --------- Stage: Development NGINX  ---------
### Proxy requests to Create React App devServer
FROM install-dependencies AS development

COPY nginx/nginx-development.conf /etc/nginx/nginx.conf

### --------- Stage: Build production bundles  ---------
FROM development as production-build

WORKDIR $CONTAINER_ROOT

COPY --chown=node:node .eslintrc.js .prettierrc ./

RUN yarn lerna:bootstrap

# Set NODE_ENV after bootstrap because we need devDependencies
# to be installed to create a production build
ENV NODE_ENV="production"

RUN yarn lerna:build --scope "@zaaksysteem/main"

### --------- Stage: Production NGINX  ---------
FROM nginx:latest as production

ARG CONTAINER_ROOT
ENV CONTAINER_ROOT $CONTAINER_ROOT

COPY nginx/nginx.conf /etc/nginx/nginx.conf

# Apps build
COPY --from=production-build $CONTAINER_ROOT/apps/main/build/ /www/data/main
