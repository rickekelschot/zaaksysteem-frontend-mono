const ERROR = 'error';
const ID_LENGTH = 2;

module.exports = {
  parser: 'babel-eslint',
  extends: ['@mintlab/react', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'id-length': [
      ERROR,
      {
        min: ID_LENGTH,
        exceptions: ['t'],
      },
    ],
    complexity: [ERROR, 6],
    'no-magic-numbers': 0,
    'no-confusing-arrow': ['off'],
    'no-inline-comments': ['off'],
    'prettier/prettier': ['error'],
  },
};
