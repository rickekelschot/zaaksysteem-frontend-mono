[⇱ Table of contents](../../)

<hr>

# `@mintlab/ui` developer documentation

> `@mintlab/ui` is an `npm` package with the 
  [presentational](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
  React UI components used in Zaaksysteem.

The [API Reference](./identifiers.html) covers component library functions and Storybook utilities.

## See also

- [Consumer Documentation](../consumer/)
- [Storybook](../../storybook/)
