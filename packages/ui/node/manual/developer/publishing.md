# Publishing to the npm registry

> Publishing is fully automated with every merge to the master branch. 

## Version increment

The package version is incremented with `semantic-release-gitlab` 
based on the VCS commit messages. No manual action is necessary.
Use `$ npm info @mintlab/ui` for the package summary or 
`$ npm info @mintlab/ui` for a list of all versions. 

## Configuration 

1. Set the `GITLAB_AUTH_TOKEN` environment variable to a
   [Personal Access Token](https://gitlab.com/profile/personal_access_tokens)
   with **api** scope. Note *personal*, unfortunately this is authentication
   where you'd only want authorization.
2. Set the `NPM_TOKEN` environment variable to an npm *Access Token*. Do **not**
   use your personal access token for that (the one that is saved to `~/.npmrc` 
   with `npm login`), create a `read and publish` token as the
   [zaaksysteem *owner* npm user](https://www.npmjs.com/settings/zaaksysteem/tokens).

Note that tokens can be invalidated by npm in response
to a security vulnerability at any time.

## Publishing manually

In case you ever need to publish manually, carefully follow these steps:

- be sure you have 
  [write access](https://www.npmjs.com/package/@mintlab/ui/access)
  for the package
- in the container, run `$ npm login` and provide your npm credentials
- because `./package.json` in the container root is symlinked, you need to
    - force copy `./root/package.json` one directory up, replacing the symlink
    - run `$ npm publish --access public`
    - revert the copied `./package.json` to a symlink again to continue development

### Why?

The problem here is that the `main` field in 
the `package.json` points to a relative path.
If it's resolved from the real file, it cannot 
be found, and your published package will be empty.

In case of doubt, it's always a good idea to run
`$ npm pack` before publishing. 
That creates the `tgz` bundle that would be published 
in the current directory, where you can inspect its contents.

## See also

- the pipeline configuration in `/.gitlab-ci.yml`.
- [Git workflow](./workflow.html)
- [`semantic-release-gitlab`](https://www.npmjs.com/package/semantic-release-gitlab)
- [`npm-publish-git-tag`](https://www.npmjs.com/package/npm-publish-git-tag)
