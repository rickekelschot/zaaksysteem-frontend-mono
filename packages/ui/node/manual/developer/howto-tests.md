# How to write tests

## Setup

- `/node
    - /jest-config.js`
        - [the Jest configuration](https://jestjs.io/docs/en/configuration)
    - `/test`
        - `/enzyme.js`
            - [enzyme setup](https://airbnb.io/enzyme/docs/installation/react-16.html)
        - `/polyfill.js`
            - fallbacks for DOM APIs that are not available in
              [jsdom](https://github.com/jsdom/jsdom)
        - `/double`  
           See 
           [Martin Fowler's TestDouble](https://martinfowler.com/bliki/TestDouble.html)
           reference if you are unfamiliar with the concept. Jest provides a lot of
           functionality for doubles out of the box, so you shouldn't need this much.
- `/App`
    - `test.js`  
      Helper functions for using enzyme.

## File system structure

Test files are located next to the *module under test* in the file system,
with the same base name and the extension `.test.js`.

The test runner finds all tests with a glob pattern, runs them and can 
create a coverage report. As such, coverage is based on your tests modules, 
not on your implementation modules. In other words, if there are no test,
there is 100% coverage.

## Module under test

In your test module, import the module under test.
If the module under test is a React component, it should
be exported with a named *and* a default export, and the 
named export should be imported in the test module.

That's because the default export is often a *Higher Order Component*
that typically injects props. Instead of adding that dependency and complexity 
to the test setup, simply set those props in the test yourself.

You still got a problem if the module under test imports the defaut export of 
another module, though. In most cases, those modules can be simply replaced 
with `jest.mock`:

    jest.mock('./path/to/ImportedComponent', () => 'span');

## See also

- [Jest CLI options](https://jestjs.io/docs/en/cli.html)
- [Jest `expect` API](https://jestjs.io/docs/en/expect)
- [Enzyne `shallow` API](https://airbnb.io/enzyme/docs/api/shallow.html)
- [Enzyne `mount` API](https://airbnb.io/enzyme/docs/api/mount.html)
- The future: 
  [Coverage with Node.js >= 10.10.0 and `c8`](https://blog.npmjs.org/post/178487845610/rethinking-javascript-test-coverage)
