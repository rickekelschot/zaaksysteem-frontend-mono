# Gitlab pages

> Built with the `pages` job in the ci pipeline.

[http://zaaksysteem.gitlab.io/npm-mintlab-ui/](http://zaaksysteem.gitlab.io/npm-mintlab-ui/)

Covers the generated HTML of

- [Storybook](https://storybook.js.org/)
    - Interactive demo and style guide
- [ESDoc](https://esdoc.org/)
    - API documentation and this manual :-) 
- [Jest](https://facebook.github.io/jest/)
    - Unit test coverage report
- [Webpack Visualizer](https://www.npmjs.com/package/webpack-visualizer-plugin)
    - Webpack bundle visualization

## Local development version

You can preview the GitLab pages artefact deploy locally
by setting a project config variable and starting
the preview server.

In the container, run

    $ npm config set @mintlab/ui:PUBLIC public/npm-mintlab-ui

This is persisted in the node user's `.npmrc` until you
rebuild the container and ensures that linking between 
artefacts works in every context the same as it does on 
the deployment server.

Now start the server:

    $ npm run rtfm

This will build all artefacts and start an HTTP server.
The container port is 8080, your host port is set to 3002
(storybook development server port + 1) by default.
