const { assign } = Object;
const { hrtime } = process;

// In most tests the theme is not used.
// https://material-ui.com/style/typography/#migration-to-typography-v2
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

function requestAnimationFrame(cb) {
  setTimeout(cb, 0);
}

assign(global, {
  requestAnimationFrame,
});
