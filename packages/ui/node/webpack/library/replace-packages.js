/**
 * Generate the main file of the published package.
 * All public modules are named exports.
 */
const { writeFileSync, readFileSync, existsSync } = require('fs');
const glob = require('glob');
const { parse, resolve, dirname } = require('path');

const APP = './App';
const GLOB = './*/*/package.json';

function resolveAbsolutePaths(file) {
  return resolve('App', file);
}

function generateSingleExport(filePath) {
  const contents = JSON.parse(readFileSync(filePath));
  const [fileName] = contents.main.split('.');

  return `
import { default as ${fileName} } from './${fileName}';
export * from './${fileName}';
export default ${fileName};
`;
}

function writeFile({ path, contents }) {
  const dir = dirname(path);
  const indexPath = `${dir}/index.js`;

  if (!existsSync(indexPath)) {
    writeFileSync(indexPath, contents);
  }
}

glob
  .sync(GLOB, { cwd: APP })
  .map(resolveAbsolutePaths)
  .map(path => ({ path, contents: generateSingleExport(path) }))
  .forEach(file => writeFile(file));
