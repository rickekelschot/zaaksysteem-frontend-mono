const WARN = 'warn';

module.exports = {
  parser: 'babel-eslint',
  extends: ['@mintlab/react', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': ['error'],
    'no-inline-comments': ['off'],
    'no-magic-numbers': ['off'],
  },
};
