import React from 'react';
import { shallow } from 'enzyme';
import { Tooltip } from '.';

/**
 * @test {Tooltip}
 */
describe('The `Tooltip` component', () => {
  test('renders its children in a div with its title', () => {
    const wrapper = shallow(
      <Tooltip classes={{}} title="Hello, world!">
        <button type="button">Click me!</button>
      </Tooltip>
    );
    const actual = wrapper.html();
    const asserted =
      '<div class="" title="Hello, world!"><button type="button">Click me!</button></div>';

    expect(actual).toBe(asserted);
  });
});
