/**
 * @return {JSS}
 */

export const dialogActionsStyleSheet = () => ({
  root: {
    margin: 0,
    padding: '16px 8px',
  },
});
