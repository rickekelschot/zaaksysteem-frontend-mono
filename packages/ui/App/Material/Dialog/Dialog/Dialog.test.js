import React from 'react';
import { shallow } from 'enzyme';
import { Dialog } from './Dialog';

const classes = {
  content: '',
};

/**
 * @test {Dialog}
 */
describe('The `Dialog` component', () => {
  test('renders nothing if its `open` prop is not truthy', () => {
    const wrapper = shallow(<Dialog classes={classes}>test</Dialog>);
    const actual = wrapper.html();
    const expected = '';

    expect(actual).toBe(expected);
  });
});
