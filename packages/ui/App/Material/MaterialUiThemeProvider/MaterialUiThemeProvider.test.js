import React from 'react';
import { shallow } from 'enzyme';
import { withTheme } from '@material-ui/core/styles';
import MaterialUiThemeProvider from '.';
import { theme as themeModule } from './library/theme';

/**
 * @test {MaterialUiThemeProvider}
 */
describe('The `MaterialUiThemeProvider` component', () => {
  test('makes the theme available to a `withTheme` HoC descendent', () => {
    function executor(resolve) {
      const App = ({ children }) => <div>{children}</div>;

      const ThemeConsumer = ({ theme }) => {
        resolve(theme);

        return null;
      };

      const ThemeConsumeHoc = withTheme()(ThemeConsumer);

      shallow(
        <MaterialUiThemeProvider>
          <App>
            <ThemeConsumeHoc />
          </App>
        </MaterialUiThemeProvider>
      ).render();
    }

    return new Promise(executor).then(theme => {
      expect(theme).toEqual(themeModule);
    });
  });
});
