/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the SpeedDial component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const speedDialStyleSheet = ({ spacing }) => ({
  speedDial: {
    position: 'absolute',
    '&$directionUp, &$directionLeft': {
      bottom: `calc(${spacing.unit}px * 2)`,
      right: `calc(${spacing.unit}px * 3)`,
    },
    '&$directionDown, &$directionRight': {
      top: `calc(${spacing.unit}px * 2)`,
      left: `calc(${spacing.unit}px * 3)`,
    },
  },
  directionUp: {},
  directionRight: {},
  directionDown: {},
  directionLeft: {},
});
