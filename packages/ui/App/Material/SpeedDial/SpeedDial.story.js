import { createElement } from 'react';
import { React, stories, boolean, select } from '../../story';
import SpeedDial from '.';
import Icon from '../Icon/Icon';

const actions = [
  {
    icon: createElement(Icon, { children: 'navigate_next' }),
    title: 'Next',
    action() {},
  },
  {
    icon: createElement(Icon, { children: 'add' }),
    title: 'Add',
    action() {},
  },
  {
    icon: createElement(Icon, { children: 'navigate_before' }),
    title: 'Previous',
    action() {},
  },
];

stories(module, __dirname, {
  Default() {
    return (
      <SpeedDial
        actions={actions}
        direction={select('Direction', ['up', 'right', 'down', 'left'])}
        hidden={boolean('Hidden', false)}
      />
    );
  },
});
