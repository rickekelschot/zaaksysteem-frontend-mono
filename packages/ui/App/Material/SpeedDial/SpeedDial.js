import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { speedDialStyleSheet } from './SpeedDial.style';
import classNames from 'classnames';
import { capitalize } from '@material-ui/core/utils/helpers';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import { addScopeAttribute } from '../../library/addScope';

/**
 * *Material Design* **SpeedDial**.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/SpeedDial
 * @see /npm-mintlab-ui/documentation/consumer/manual/SpeedDial.html
 * @see https://material-ui.com/lab/api/speed-dial/
 *
 * @reactProps {Object} classes
 * @reactProps {Array} actions
 * @reactProps {string} direction
 * @reactProps {boolean} hidden
 */
class SpeedDials extends React.Component {
  state = {
    open: false,
  };

  render() {
    const {
      props: { classes, actions, hidden, scope, direction = 'up' },
      state: { open },
    } = this;

    const speedDialClassName = classNames(
      classes.speedDial,
      classes[`direction${capitalize(direction)}`]
    );

    return (
      <SpeedDial
        ariaLabel="SpeedDial example"
        className={speedDialClassName}
        hidden={hidden}
        icon={<SpeedDialIcon />}
        onBlur={this.handleClose}
        onClick={this.handleClick}
        onClose={this.handleClose}
        onFocus={this.handleOpen}
        onMouseEnter={this.handleOpen}
        onMouseLeave={this.handleClose}
        open={open}
        direction={direction}
        ButtonProps={{ ...addScopeAttribute(scope, 'speeddial', 'button') }}
      >
        {actions.map(({ title, icon, action }) => (
          <SpeedDialAction
            key={title}
            icon={icon}
            tooltipTitle={title}
            onClick={() => {
              this.handleClick();
              action();
            }}
            {...addScopeAttribute(scope, 'speeddial', title, 'button')}
          />
        ))}
      </SpeedDial>
    );
  }

  handleClick = () => {
    this.setState(state => ({
      open: !state.open,
    }));
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };
}

export default withStyles(speedDialStyleSheet)(SpeedDials);
