import React from 'react';
import { shallow, mount } from 'enzyme';
import Breadcrumbs from './Breadcrumbs';
import MaterialUiThemeProvider from '../MaterialUiThemeProvider/MaterialUiThemeProvider';

const ONE = 1;
const FOUR = 4;

describe('The `Breadcrumbs` component', () => {
  const items = [
    {
      label: 'Home',
      path: '/',
    },
    {
      label: 'Level 1',
      path: '/level',
    },
    {
      label: 'Level 2',
      path: '/level/level',
    },
    {
      label: 'Level 3',
      path: '/level/level/level',
    },
    {
      label: 'Level 4',
      path: '/level/level/level/level',
    },
  ];

  test('renders with crashing', () => {
    const wrapper = shallow(<Breadcrumbs items={items} />);
    expect(wrapper).toHaveLength(ONE);
  });

  describe('with custom renderer functions', () => {
    test('are executed', () => {
      const itemRenderer = jest.fn();
      const lastItemRenderer = jest.fn();

      mount(
        <MaterialUiThemeProvider>
          <Breadcrumbs
            items={items}
            itemRenderer={itemRenderer}
            lastItemRenderer={lastItemRenderer}
          />
        </MaterialUiThemeProvider>
      );

      expect(itemRenderer.mock.calls.length).toBe(FOUR);
      expect(lastItemRenderer.mock.calls.length).toBe(ONE);
    });
  });
});
