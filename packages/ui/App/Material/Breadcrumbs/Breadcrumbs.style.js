/**
 * Style Sheet for the {@link Button} component custom `semiContained` variant.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const breadcrumbStylesheet = ({ palette: { common }, typography }) => {
  const item = {
    ...typography.h4,
    color: common.black,
    opacity: 0.4,
  };

  return {
    item,
    separator: {
      fill: common.black,
      opacity: 0.4,
    },
    link: {
      ...item,
      textDecoration: 'none',
      '&:hover': {
        opacity: 1,
      },
    },
    last: {
      ...typography.h4,
      color: common.black,
    },
  };
};
