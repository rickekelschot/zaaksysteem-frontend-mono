const ZERO = 0;

const biggerThan = num => value => value > num;

/**
 * @param {Array<Object>} items
 * @param {Number} maxItems
 * @return {Array<Object>}
 */
export const getItemsToRender = (items, maxItems) => {
  if (items.length >= maxItems) {
    const isLastTwoItems = biggerThan(items.length - maxItems);
    const [first, second, ...rest] = items.filter(
      (item, index) => index === ZERO || isLastTwoItems(index)
    );

    return [
      first,
      {
        ...second,
        label: '...',
      },
      ...rest,
    ];
  }

  return items;
};
