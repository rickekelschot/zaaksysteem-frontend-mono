# 🔌 `Card` component

> *Material Design* **Card**.

## Example

    <Card
      title="My title"
      description="My description."
    >My content.</Card>

## See also

- [`Card` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Card)
- [`Card` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-card)

## External resources

- [*Material Guidelines*: Cards](https://material.io/design/components/cards.html)
- [*Material-UI* `Card` API](https://material-ui.com/api/card/)
