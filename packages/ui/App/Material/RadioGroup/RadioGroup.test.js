import React from 'react';
import { mount, shallow } from 'enzyme';
import RadioGroup from '.';
import { fill } from '../../test';

/**
 * @test {RadioGroup}
 */
describe('The `RadioGroup` component', () => {
  test('renders a Radio component for each `choices` configuration', () => {
    const count = 7;
    const wrapper = mount(
      <RadioGroup
        choices={fill(count, {
          label: 'foo',
          value: 'bar',
        })}
      />
    );
    const actual = wrapper.find('Radio').length;
    const asserted = count;

    expect(actual).toBe(asserted);
  });

  test('has an `handleChange` method that sets its internal state value', () => {
    const wrapper = shallow(<RadioGroup choices={[]} />);
    const instance = wrapper.instance();
    const value = 'MY VALUE';

    instance.handleChange({
      target: {
        value,
      },
    });

    const actual = instance.state.value;
    const asserted = value;

    expect(actual).toBe(asserted);
  });
});
