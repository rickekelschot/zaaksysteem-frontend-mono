import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import * as iconMap from './library';

/**
 * *Material Design* **Icon**.
 * - facade for a subset of `@material-ui/icons`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Icon
 * @see /npm-mintlab-ui/documentation/consumer/manual/Icon.html
 * @see https://material-ui.com/api/icon/
 *
 * @param {Object} props
 * @param {string} props.children
 * @param {Object} [props.classes]
 * @param {string} [props.color]
 * @param {string} [props.size=medium]
 * @return {ReactElement}
 */
export const Icon = ({
  children,
  classes,
  color,
  size = 'medium',
  theme: {
    mintlab: { icon },
  },
}) => {
  const IconComponent = iconMap[children];

  return (
    <span
      style={{
        display: 'inline-flex',
        fontSize: `${icon[size]}px`,
      }}
    >
      <IconComponent classes={classes} color={color} fontSize="inherit" />
    </span>
  );
};

export default withTheme()(Icon);
