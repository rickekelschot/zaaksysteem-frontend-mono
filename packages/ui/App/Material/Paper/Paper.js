import React from 'react';
import MuiPaper from '@material-ui/core/Paper';
import { addScopeAttribute } from '../../library/addScope';

/**
 * Facade for *Material-UI* `Paper`
 * - all `props` are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Paper
 * @see /npm-mintlab-ui/documentation/consumer/manual/Paper.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {React.Children} props.children
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
const Paper = props => {
  const { classes, children, scope, ...rest } = props;

  return (
    <MuiPaper
      classes={classes}
      {...addScopeAttribute(scope, 'paper')}
      {...rest}
    >
      {children}
    </MuiPaper>
  );
};

export default Paper;
