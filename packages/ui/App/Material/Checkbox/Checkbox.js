import React from 'react';
import MuiCheckbox from '@material-ui/core/Checkbox';
import MuiFormControlLabel from '@material-ui/core/FormControlLabel';
import { addScopeAttribute } from '../../library/addScope';
/**
 * *Material Design* **Checkbox** selection control.
 * - facade for *Material-UI* `Checkbox`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Checkbox
 * @see /npm-mintlab-ui/documentation/consumer/manual/Checkbox.html
 *
 * @param {Object} props
 * @param {boolean} [props.checked=false]
 * @param {boolean} [props.disabled=false]
 * @param {string} [props.label]
 * @param {string} [props.name]
 * @param {string} [props.color='primary']
 * @param {Function} [props.onChange]
 * @param {Function} [props.onClick]
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Checkbox = ({
  checked = false,
  formControlClasses,
  disabled = false,
  label,
  name,
  color = 'primary',
  onChange,
  onClick,
  scope,
}) => (
  <MuiFormControlLabel
    classes={formControlClasses}
    onClick={onClick}
    control={
      <MuiCheckbox
        checked={checked}
        disabled={disabled}
        name={name}
        color={color}
        onChange={onChange}
        inputProps={{
          ...addScopeAttribute(scope, 'checkbox'),
        }}
      />
    }
    label={label}
  />
);

export default Checkbox;
