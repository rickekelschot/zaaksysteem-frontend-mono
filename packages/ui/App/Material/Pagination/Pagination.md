# 🔌 `Pagination` component

> Facade for *Material UI* `TablePagination`.

## See also

- [`Pagination` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Pagination)
- [`Pagination` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-pagination)
