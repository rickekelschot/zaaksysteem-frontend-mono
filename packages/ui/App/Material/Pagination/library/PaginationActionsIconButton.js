import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { pageExists } from './functions';
import { addScopeAttribute } from '../../../library/addScope';

/**
 * *Material UI* `IconButton` facade component for pagination.
 *
 * @param {Object} props
 * @param {Object} props.button
 * @param {Number} props.button.destination
 * @param {Function} props.button.icon
 * @param {Object} props.button.buttonClasses
 * @param {string} [props.button.scope]
 * @param {Number} props.page
 * @param {Number} props.pageCount
 * @param {Function} props.onChangePage
 * @return {ReactElement}
 */
export const PaginationActionsIconButton = ({
  button: { destination, icon, buttonClasses, scope },
  onChangePage,
  page,
  pageCount,
}) => (
  <IconButton
    classes={buttonClasses}
    disabled={!pageExists(destination, pageCount) || destination === page}
    onClick={() => {
      onChangePage(destination);
    }}
    {...addScopeAttribute(scope, 'button')}
  >
    {icon}
  </IconButton>
);

export default PaginationActionsIconButton;
