import { Snackbar } from '.';
import { getShallowInstance } from '../../test';

const { assign } = Object;

const getInstance = props =>
  getShallowInstance(
    Snackbar,
    assign(
      {
        classes: {},
      },
      props
    )
  );

/**
 * @test {Snackbar}
 */
describe('The `Snackbar` component', () => {
  describe('has a `componentDidUpdate` lifecycle method', () => {
    test('pushes new props to the queue', () => {
      const instance = getInstance();

      instance.componentDidUpdate({
        someUpdate: true,
      });

      const actual = instance.queue.length;
      const asserted = 1;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `handleClose` method', () => {
    test('that does not set `state.open` if the event reason is `clickaway`', () => {
      const instance = getInstance();

      instance.handleClose(null, 'clickaway');

      const actual = instance.state.open;
      const asserted = true;

      expect(actual).toBe(asserted);
    });

    test('that sets `state.open` if the event reason is not `clickaway`', () => {
      const instance = getInstance();
      instance.handleClose(null, 'Hello, sailor!');

      const actual = instance.state.open;
      const asserted = false;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `processQueue` method', () => {
    test('that calls `onQueueEmpty` if the queue is empty', () => {
      const spy = jest.fn();

      const instance = getInstance({
        onQueueEmpty: spy,
      });

      const assertedTimesCalled = 1;
      instance.processQueue();
      expect(spy).toHaveBeenCalledTimes(assertedTimesCalled);
    });
  });

  test('has a `getCloneWithout` method that passes filtered props to the Material-UI component', () => {
    const instance = getInstance();

    const props = {
      onQueueEmpty: '',
      onClose: '',
      onExited: '',
      open: '',
      action: '',
      foo: 'bar',
    };

    const actual = instance.getCloneWithout(props);
    const asserted = {
      foo: 'bar',
    };

    expect(actual).toEqual(asserted);
  });
});
