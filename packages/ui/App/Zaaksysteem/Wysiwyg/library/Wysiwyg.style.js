/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const wysiwygStyleSheet = ({
  palette: { error },
  typography: { fontFamily },
  mintlab: { greyscale, radius },
}) => ({
  defaultClass: {
    backgroundColor: greyscale.light,
    '& .rdw-option-wrapper': {
      backgroundColor: greyscale.light,
    },
    borderRadius: radius.wysiwyg,
    fontFamily,
  },
  errorClass: {
    backgroundColor: error.light,
    '& .rdw-option-wrapper': {
      backgroundColor: error.light,
    },
  },
  editorClass: {
    padding: '10px',
  },
});
