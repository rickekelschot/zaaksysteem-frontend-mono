import React from 'react';
import { shallow } from 'enzyme';
import { Loader } from '.';

const theme = {
  palette: {
    common: {
      black: '#000',
    },
  },
};

/**
 * @test {Loader}
 */
describe('The `Loader` component', () => {
  test('renders a loader if its `active` props is truthy', () => {
    const wrapper = shallow(
      <Loader active={true} theme={theme}>
        TEST
      </Loader>
    );
    const actual = wrapper.text();
    const unasserted = 'TEST';

    expect(actual).not.toBe(unasserted);
  });

  test('renders its children if its `active` props is falsy', () => {
    const wrapper = shallow(
      <Loader active={false} theme={theme}>
        TEST
      </Loader>
    );
    const actual = wrapper.text();
    const asserted = 'TEST';

    expect(actual).toBe(asserted);
  });

  test('defaults to the Circle loader', () => {
    const wrapper = shallow(
      <Loader active={true} theme={theme}>
        TEST
      </Loader>
    );
    const actual = wrapper.find('Circle').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });

  test('has a cube loader type', () => {
    const wrapper = shallow(
      <Loader active={true} color="#012" theme={theme} type="cube">
        TEST
      </Loader>
    );
    const actual = wrapper.find('Cube').length;
    const asserted = 1;

    expect(wrapper.find('Cube').props().color).toBe('#012');
    expect(actual).toBe(asserted);
  });

  test('has a fold loader type', () => {
    const wrapper = shallow(
      <Loader active={true} theme={theme} type="fold">
        TEST
      </Loader>
    );
    const actual = wrapper.find('Fold').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });

  test('has a pulse loader type', () => {
    const wrapper = shallow(
      <Loader active={true} theme={theme} type="pulse">
        TEST
      </Loader>
    );
    const actual = wrapper.find('Pulse').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });

  test('has a wave loader type', () => {
    const wrapper = shallow(
      <Loader active={true} theme={theme} type="wave">
        TEST
      </Loader>
    );
    const actual = wrapper.find('Wave').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });
});
