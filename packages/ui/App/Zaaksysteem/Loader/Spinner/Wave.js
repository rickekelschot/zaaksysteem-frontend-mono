import React from 'react';
import { iterator } from '../library/iterator';
import style from './Wave.css';
import { addScopeAttribute } from '../../../library/addScope';

const LENGTH = 5;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Wave = ({ color, scope }) => (
  <div className={style['wave']} {...addScopeAttribute(scope)}>
    {iterator(LENGTH).map(item => (
      <div
        key={item}
        className={style['rect']}
        style={{
          backgroundColor: color,
        }}
      />
    ))}
  </div>
);
