import React, { createElement } from 'react';
import { withTheme } from '@material-ui/core/styles';
import ReactSelect from 'react-select';
import { bind, cloneWithout } from '@mintlab/kitchen-sink/source';
import selectStyleSheet from './Shared.style';
import SelectBase from '../Select.base';
import ClearIndicator from '../library/ClearIndicator';
import DropdownIndicator from '../library/DropdownIndicator';

const { assign } = Object;

/**
 * Choices and value must be provided in the form of a single, or array of objects.
 *
 * @typedef {Object} SelectValue
 * @property {string} value
 * @property {string} label
 * @property {Array<string>} alternativeLabels
 * @example
 * {
 *   value: 'strawberry',
 *   label: 'Strawberry',
 *   alternativeLabels: ['Fruit', 'Red'],
 * }
 */

/**
 * Facade for React Select v2.
 * - additional props are passed through to that component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Select
 * @see /npm-mintlab-ui/documentation/consumer/manual/Select.html
 * @see https://react-select.com/home
 *
 * @reactProps {boolean} [autoLoad=false]
 * @reactProps {SelectValue|Array<SelectValue>} choices
 * @reactProps {boolean} [disabled=false]
 * @reactProps {Function} filterOption
 * @reactProps {Function} getChoices
 * @reactProps {string} error
 * @reactProps {string} name
 * @reactProps {Function} [onChange]
 * @reactProps {Object} styles
 * @reactProps {Object} translations
 * @reactProps {SelectValue|Array<SelectValue>} value
 * @reactProps {boolean} loading
 */
export class Select extends SelectBase {
  constructor(props) {
    super(props);
    bind(this, 'getDropdownIndicator');
  }

  /* eslint complexity: [2, 7] */

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        choices: options,
        error,
        name,
        translations,
        theme,
        disabled,
        autoLoad,
        styles,
        hasInitialChoices,
        value,
        filterOption,
        loading,
        ...rest
      },
      handleInputChange,
      handleFocus,
      handleChange,
      handleBlur,
      handleKeyDown,
      getDropdownIndicator,
    } = this;

    const shouldHandleInputChange = () => !autoLoad && !hasInitialChoices;

    const whichStyles = () =>
      styles ||
      selectStyleSheet({
        theme,
        error,
      });

    return (
      <ReactSelect
        isDisabled={disabled}
        isLoading={loading}
        cacheOptions={true}
        value={value}
        options={options}
        loadingMessage={() => translations['form:loading']}
        noOptionsMessage={() => null}
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onKeyDown={handleKeyDown}
        onInputChange={shouldHandleInputChange() && handleInputChange}
        placeholder={translations['form:choose']}
        styles={whichStyles()}
        classNamePrefix="react-select"
        name={name}
        id={name}
        filterOption={filterOption}
        components={{
          ClearIndicator,
          DropdownIndicator: getDropdownIndicator,
        }}
        {...cloneWithout(
          rest,
          'name',
          'value',
          'onBlur',
          'onChange',
          'onFocus',
          'onKeyDown'
        )}
      />
    );
  }

  /**
   *
   * @param {Object} selectProps
   * @return {ReactElement}
   */
  getDropdownIndicator(selectProps) {
    const { choices } = this.props;

    if (!choices || (Array.isArray(choices) && !choices.length)) {
      return null;
    }

    return createElement(DropdownIndicator, assign({}, selectProps));
  }
}

export default withTheme()(Select);
