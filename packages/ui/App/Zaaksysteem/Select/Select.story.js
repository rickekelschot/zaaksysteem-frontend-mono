import { React, stories, text, boolean } from '../../story';
import Icon from '../../Material/Icon';
import Select from '.';

const DELAY = 2500;

const defaultChoices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
    alternativeLabels: ['Yummy', 'I love this flavor'],
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
    alternativeLabels: ['Fruit', 'Red'],
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
  {
    value: 'knackebrod',
    label: 'KNÄCKEBRÖD RÅG',
    alternativeLabels: [
      'hrökkbrauð',
      'knækbrød',
      'knekkebrød',
      'hårt bröd',
      'näkkileipä',
    ],
  },
];

const choicesCreatable = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const dropdownOptionsCreatable = [
  {
    value: '1',
    label: 'Regular text',
  },
  {
    value: '2',
    label: 'Súdwest-Fryslân',
  },
  {
    value: '4',
    label: 'KNÄCKEBRÖD RÅG',
  },
];

const [, defaultValue] = defaultChoices;

const stores = {
  Form: {
    value: defaultValue,
  },
  Generic: {
    value: null,
    choices: [],
  },
  Creatable: {
    value: choicesCreatable,
  },
};

/**
 * @param {SyntheticEvent} event
 * @param {Object} store
 * @return {undefined}
 */
const onChange = ({ value }, store) => store.set({ value });

stories(
  module,
  __dirname,
  {
    Form({ store, value }) {
      return (
        <div
          style={{
            width: '15rem',
          }}
        >
          <Select
            choices={defaultChoices}
            error={text('Error', '')}
            isMulti={boolean('Multiple choices', false)}
            value={value}
            onChange={event => onChange(event, store)}
            name="story"
            id="story"
            isClearable={boolean('Clearable', true)}
            loading={boolean('Loading', false)}
            scope="story"
          />
        </div>
      );
    },
    Generic({ store, choices, value }) {
      const startAdornment = boolean('Start adornment', true) ? (
        <Icon size="small">people</Icon>
      ) : null;

      return (
        <div
          style={{
            width: '15rem',
          }}
        >
          <Select
            generic={true}
            choices={choices}
            loading={!choices.length}
            autoLoad={true}
            getChoices={() => {
              setTimeout(() => {
                store.set({
                  choices: defaultChoices,
                });
              }, DELAY);
            }}
            value={value}
            onChange={event => onChange(event, store)}
            name="story"
            startAdornment={startAdornment}
            isClearable={boolean('Clearable', true)}
          />
        </div>
      );
    },
    Creatable({ store, value }) {
      return (
        <div
          style={{
            width: '30rem',
          }}
        >
          <Select
            creatable={true}
            error={text('Error', '')}
            value={value}
            onChange={event => onChange(event, store)}
            options={dropdownOptionsCreatable}
            name="story"
          />
        </div>
      );
    },
  },
  stores
);
