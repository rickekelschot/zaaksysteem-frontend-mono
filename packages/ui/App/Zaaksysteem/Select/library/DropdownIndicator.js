import React from 'react';
import { components } from 'react-select';
import DropdownIndicatorButton from '../../../Shared/DropdownIndicator/DropdownIndicator';

/**
 * @param {Object} props
 * @return {ReactElement}
 */
const DropdownIndicator = props => (
  <components.DropdownIndicator {...props}>
    <DropdownIndicatorButton />
  </components.DropdownIndicator>
);

export default DropdownIndicator;
