import { createElement } from 'react';
import FormSelect from '../Form/Select';
import CreatableSelect from '../Form/CreatableSelect';
import GenericSelect from '../Generic/GenericSelect';

/**
 * @param {Object} props
 * @param {boolean} props.creatable
 * @param {boolean} props.generic
 * @return {ReactElement}
 */
export const SelectStrategy = ({
  creatable = false,
  generic = false,
  ...rest
}) => {
  if (creatable) {
    return createElement(CreatableSelect, rest);
  }

  if (generic) {
    return createElement(GenericSelect, rest);
  }

  return createElement(FormSelect, rest);
};

export default SelectStrategy;
