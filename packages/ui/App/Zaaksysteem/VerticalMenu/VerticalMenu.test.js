import React from 'react';
import { mount } from 'enzyme';
import VerticalMenu from '.';

/**
 * @test {VerticalMenu}
 */
xdescribe('The `VerticalMenu` component', () => {
  describe('The `DropdownMenu` component', () => {
    test('renders a button for each item', () => {
      const items = [{ label: 'foo' }, { label: 'bar' }];
      const wrapper = mount(<VerticalMenu items={items} />);
      const actual = wrapper.find('nav button').length;
      const expected = 2;

      expect(actual).toBe(expected);
    });
  });
});
