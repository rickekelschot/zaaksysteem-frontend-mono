/**
 * Style Sheet for the {@link DropdownMenuList} component
 * @return {JSS}
 */
export const dropdownMenuListStylesheet = () => ({
  list: {
    '& button:not(:last-child)': {
      marginBottom: '6px',
    },
  },
});
