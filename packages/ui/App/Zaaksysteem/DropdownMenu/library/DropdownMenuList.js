import React from 'react';
import Divider from '@material-ui/core/Divider';
import DropdownMenuButton from './DropdownMenuButton';
import { dropdownMenuListStylesheet } from './DropdownMenuList.style';
import { withStyles } from '@material-ui/core/styles';
import { addScopeProp } from '../../../library/addScope';

const { isArray } = Array;

/**
 * Returns a wrapper with one or more groups of DropdownMenuButton components.
 * Groups are seperated by a Divider component.
 *
 * @param {Array<Action>|Array<Array<Action>>} items
 * @param {Object} classes
 * @return {ReactElement}
 */
const DropdownMenuList = ({ items, classes, scope }) => {
  const normalized = items.every(item => isArray(item)) ? items : [items];

  return normalized.map((group, index) => (
    <div key={index} className={classes.list}>
      {Boolean(index) && index !== normalized.length && <Divider />}
      {group.map(({ label, action, icon }, buttonIndex) => (
        <DropdownMenuButton
          key={buttonIndex}
          label={label}
          action={action}
          icon={icon}
          {...addScopeProp(scope, label)}
        />
      ))}
    </div>
  ));
};

export default withStyles(dropdownMenuListStylesheet)(DropdownMenuList);
