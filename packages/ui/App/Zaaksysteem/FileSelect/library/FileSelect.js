import React from 'react';
import Dropzone from 'react-dropzone';
import classnames from 'classnames';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@material-ui/core/styles';
import Render from '../../../Abstract/Render/Render';
import Button from '../../../Material/Button/Button';
import { Subtitle1, Caption } from '../../../Material/Typography';
import { fileSelectStylesheet } from './FileSelect.style';
import { addScopeProp } from '../../../library/addScope';

/* eslint complexity: [2, 5] */
export const FileSelect = ({
  selectInstructions,
  dragInstructions,
  dropInstructions,
  orLabel,
  onDrop,
  classes,
  fileList,
  hasFiles,
  error,
  scope,
  accept,
  multiple = true,
}) => (
  <Dropzone onDrop={onDrop} accept={accept}>
    {({ getRootProps, getInputProps, isDragActive, open }) => {
      const inputProps = getInputProps({
        multiple,
      });
      const rootProps = getRootProps();

      return (
        <div
          className={classnames(
            classes.wrapper,
            Boolean(error) && classes.error
          )}
          {...cloneWithout(rootProps, 'onClick', 'tabIndex')}
        >
          <input {...inputProps} />

          <div className={classes.fileList}>{fileList}</div>

          <div>
            <div className={classes.form}>
              <Render condition={!hasFiles}>
                <Subtitle1>
                  {isDragActive ? dropInstructions : dragInstructions}
                </Subtitle1>
                <div className={classes.orLabel}>
                  <Caption>- {orLabel} -</Caption>
                </div>
              </Render>
              <Render condition={!hasFiles || multiple}>
                <Button
                  presets={['primary', 'semiContained']}
                  action={open}
                  {...addScopeProp(scope, 'file-select', 'select')}
                >
                  {selectInstructions}
                </Button>
              </Render>
            </div>
          </div>
        </div>
      );
    }}
  </Dropzone>
);

export default withStyles(fileSelectStylesheet)(FileSelect);
