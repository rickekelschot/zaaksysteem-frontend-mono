import React from 'react';
import LazyLoader from '../../../Abstract/LazyLoader/LazyLoader';

/**
 * Create a single `file-select` based chunk.
 *
 * Depends on {@link LazyLoader}.
 *
 * @param {Object} props
 * @return {ReactElement}
 */

export const FileSelect = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "ui.file-select" */
        './FileSelect'
      )
    }
    {...props}
  />
);

export default FileSelect;
