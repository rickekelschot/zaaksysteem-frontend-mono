/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Wywisyg component
 * @param {Object} theme
 * @return {Object}
 */
export const fileListStylesheet = () => ({
  wrapper: {
    width: '100%',
    '& > *': {
      marginBottom: 8,
    },
  },
});
