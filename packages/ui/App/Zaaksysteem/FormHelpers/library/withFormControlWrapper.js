import React from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '../FormControlWrapper';

/**
 * @param {ReactComponent} WrappedComponent
 * @returns {ReactComponent}
 */
export const withFormControlWrapper = WrappedComponent => {
  return function WithFormControlWrapper({
    error,
    help,
    hint,
    label,
    required,
    compact,
    scope,
    touched,
    classes,
    ...rest
  }) {
    return (
      <FormControlWrapper
        label={label}
        error={error}
        help={help}
        hint={hint}
        required={required}
        compact={compact}
        scope={scope}
        touched={touched}
        classes={classes}
      >
        <WrappedComponent
          label={label}
          error={error}
          help={help}
          hint={hint}
          required={required}
          {...cloneWithout(rest, 'type')}
        />
      </FormControlWrapper>
    );
  };
};

export default withFormControlWrapper;
