import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';
import CompactButton from './CompactButton';
import { permanentDrawerStyleSheet } from './PermanentDrawer.style';
import { addScopeProp } from '../../../../library/addScope';

const ACTIVE_DEFAULT = 0;

/**
 * @param {number} [active=0]
 * @param {Object} classes
 * @param {string} className
 * @param {Array} navigation
 * @return {ReactElement}
 */
export const PermanentDrawer = ({
  active = ACTIVE_DEFAULT,
  classes,
  className,
  navigation,
  scope,
}) => (
  <Drawer
    classes={{
      paper: classes.paper,
    }}
    className={className}
    variant="permanent"
  >
    <nav>
      <ul className={classes.list}>
        {navigation.map(({ action, icon, label }, index) => (
          <li key={index}>
            <CompactButton
              action={() => action(label)}
              active={index === active}
              icon={icon}
              label={label}
              {...addScopeProp(scope, label)}
            />
          </li>
        ))}
      </ul>
    </nav>
  </Drawer>
);

export default withStyles(permanentDrawerStyleSheet)(PermanentDrawer);
