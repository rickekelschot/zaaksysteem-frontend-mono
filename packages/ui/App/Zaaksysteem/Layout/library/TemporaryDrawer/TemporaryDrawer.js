import React, { Fragment } from 'react';
import ButtonBase from '@material-ui/core/ButtonBase';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';
import MenuGroup from './MenuGroup';
import Render from '../../../../Abstract/Render';
import { Subtitle2 } from '../../../../Material/Typography';
import { temporaryDrawerStyleSheet } from './TemporaryDrawer.style';
import { addScopeProp } from '../../../../library/addScope';

const TemporaryDrawer = ({
  active,
  classes,
  navigation,
  open,
  subtitle,
  title,
  toggle,
  scope,
}) => (
  <Drawer
    classes={{
      paper: classes.paper,
    }}
    onClose={toggle}
    open={open}
    variant="temporary"
    ModalProps={{
      hideBackdrop: true,
      onClick: toggle,
    }}
  >
    <div className={classes.titles}>
      <Subtitle2
        classes={{
          root: classes.title,
        }}
      >
        {title}
      </Subtitle2>
      <div className={classes.subtitle}>{subtitle}</div>
    </div>

    <Render condition={navigation.primary}>
      <MenuGroup
        navigation={navigation.primary}
        active={active}
        classes={{
          menuList: classes.primary,
        }}
        {...addScopeProp(scope, 'primary')}
      />
    </Render>

    <Render condition={navigation.secondary}>
      <Fragment>
        <Divider />
        <MenuGroup
          navigation={navigation.secondary}
          classes={{
            menuList: classes.secondary,
          }}
          {...addScopeProp(scope, 'secondary')}
        />
      </Fragment>
    </Render>

    <Render condition={navigation.about}>
      <ButtonBase
        classes={{
          root: classes.about,
        }}
        onClick={navigation.about.action}
      >
        {navigation.about.label}
      </ButtonBase>
    </Render>
  </Drawer>
);

export default withStyles(temporaryDrawerStyleSheet)(TemporaryDrawer);
