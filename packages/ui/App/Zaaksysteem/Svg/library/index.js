export { getStyle } from './getStyle';
export { isResponsiveStyle } from './isResponsiveStyle';
export { toViewBox } from './toViewBox';
