# 🔌 `Dynamic` component

> Pass a component dynamically with a property.

## Usage

### String (HTML elements)

    <Dynamic
      component="span"
      title="Foobar"
    >Hello, world!</Dynamic> 

### Function

    <Dynamic
      component={Fubar}
      title="Foobar"
    >Hello, world!</Dynamic> 

## See also

- [`Dynamic` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/Dynamic)
- [`Dynamic` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#abstract-dynamic)
