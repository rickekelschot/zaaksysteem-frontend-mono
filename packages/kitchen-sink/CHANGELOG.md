# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@mintlab/kitchen-sink@3.3.0...@mintlab/kitchen-sink@3.3.1) (2019-07-23)

**Note:** Version bump only for package @mintlab/kitchen-sink





# 3.3.0 (2019-07-18)


### Features

* **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
* **KitchenSink:** MINTY-1120 Import kitchen-sink package into monorepo ([ee5f0bb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/ee5f0bb))
* **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
