import { get } from './object';
const { keys } = Object;

/**
 * Get the path segments from an absolute path with an optional query.
 *
 * @param {string} path
 * @return {Array}
 */
export function getSegments(path) {
  const [pathComponent] = path.split('?');
  const [, ...segments] = pathComponent.split('/');

  return segments;
}

/**
 * Get the second path segment from an absolute path with an optional query.
 *
 * @param {string} path
 * @return {string}
 */
export function getSegment(path) {
  const [, segment] = getSegments(path);

  return segment;
}

/**
 * Build a paramString from an the keyValues of an object
 *
 * @param {Object} params
 * @return {string}
 */
export function buildParamString(params) {
  const paramsAsString = keys(params)
    .filter(param => params[param])
    .map(param => `${param}=${encodeURIComponent(get(params, param))}`)
    .join('&');
  const preParam = paramsAsString.length ? '?' : '';

  return `${preParam}${paramsAsString}`;
}

/**
 * Build a URL from the base url and given params
 *
 * @param {string} url
 * @param {Object} params
 * @return {string}
 */
export function buildUrl(url, params) {
  return `${url}${buildParamString(params)}`;
}

/**
 * @param {string} params
 * @return {Object}
 */
export const objectifyParams = params => {
  if (!params) {
    return {};
  }

  return params.split('&').reduce((acc, param) => {
    const [name, value] = param.split('=');

    acc[name] = decodeURIComponent(value);

    return acc;
  }, {});
};
