export const capitalize = string =>
  string.replace(/^./, function Upper(match) {
    return match.toUpperCase();
  });
