module.exports = async ({ config, mode }) => {
  const babelLoader = config.module.rules.find(rule =>
    /babel-loader/.test(rule.loader)
  );

  babelLoader.include = [babelLoader.include, /apps/, /packages/, /@mintlab/];
  config.node = {
    __dirname: true,
  };

  return config;
};
