const createReactAppOverrides = require('@zaaksysteem/common/src/createReactAppOverrides');
const package = require('./package.json');

const basePath =
  process.env.DOCKER === 'true' || process.env.NODE_ENV === 'production'
    ? `${package.homepage}/`
    : '/';

module.exports = createReactAppOverrides(basePath);
