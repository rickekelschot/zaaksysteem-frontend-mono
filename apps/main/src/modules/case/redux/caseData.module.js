import { caseDataReducer } from './caseData.reducer';

export function getCaseDataModule() {
  return {
    id: 'caseData',
    reducerMap: {
      caseData: caseDataReducer,
    },
  };
}
