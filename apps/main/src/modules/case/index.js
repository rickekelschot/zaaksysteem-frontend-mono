import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import Loadable from 'react-loadable';
import { getCaseDataModule } from './redux/caseData.module';

const Loading = () => <div>Loading</div>;

const LoadableComponent = Loadable({
  loader: () => import('./component/CaseContainer'),
  loading: Loading,
});

export default function CaseDynamic() {
  return (
    <DynamicModuleLoader modules={[getCaseDataModule()]}>
      <LoadableComponent />
    </DynamicModuleLoader>
  );
}
