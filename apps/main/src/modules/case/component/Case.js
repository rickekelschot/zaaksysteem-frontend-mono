import React from 'react';
import { H1 } from '@mintlab/ui/App/Material/Typography';
import Wysiwyg from '@mintlab/ui/App/Zaaksysteem/Wysiwyg';

const Case = ({ uuid }) => (
  <React.Fragment>
    <H1>Hello as {uuid}</H1>
    <Wysiwyg />
  </React.Fragment>
);

export default Case;
