import { connect } from 'react-redux';
import Case from './Case';

const CaseContainer = connect(({ caseData: { uuid } }) => ({ uuid }))(Case);

export default CaseContainer;
