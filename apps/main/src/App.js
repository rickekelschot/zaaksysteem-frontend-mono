import React from 'react';
import { hot } from 'react-hot-loader/root';
import { setConfig } from 'react-hot-loader';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
import CaseDynamic from './modules/case';

setConfig({
  logLevel: 'debug',
});

function App() {
  return (
    <MaterialUiThemeProvider>
      <Router>
        <Route path="*" component={CaseDynamic} />
      </Router>
    </MaterialUiThemeProvider>
  );
}

export default process.env.NODE_ENV === 'development' ? hot(App) : App;
